import { UserPoolDescriptionType } from 'aws-sdk/clients/cognitoidentityserviceprovider';
export * from './lambdas/createCustomer/types';
export * from './lambdas/listCustomers/types';
export * from './lambdas/deleteCustomer/types';
export * from './lambdas/getCustomerByName/types';
export declare const cognitoCustomerToCustomer: (customer: UserPoolDescriptionType) => {
    customerId: string;
    customerName: string;
    enabled: boolean;
};
export declare type Customer = {
    customerId: string;
    customerName: string;
    clientId?: string;
    enabled: boolean;
};
