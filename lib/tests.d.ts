import * as createCustomerTests from './lambdas/createCustomer/createCustomerTests';
import * as listCustomerTests from './lambdas/listCustomers/listCustomersTests';
import * as deleteCustomerTests from './lambdas/deleteCustomer/deleteCustomerTests';
import * as getCustomerByNameTests from './lambdas/getCustomerByName/getCustomerByNameTests';
export { createCustomerTests, listCustomerTests, deleteCustomerTests, getCustomerByNameTests };
