import { ListCustomersResponse, CreateCustomerResponse, CreateCustomerRequest, DeleteCustomerResponse } from '../../Types';
export declare type DeleteCustomerTestObject = {
    createCustomerRequest: CreateCustomerRequest;
    createCustomerResponse: CreateCustomerResponse;
    deleteCustomerResponse: DeleteCustomerResponse;
    listCustomersResponse: ListCustomersResponse;
    error: Error;
};
export declare const createTestObject: () => {
    createCustomerRequest: {
        customerName: string;
        passwordPolicy: {
            MinimumLength: number;
            RequireLowercase: boolean;
            RequireSymbols: boolean;
            RequireUppercase: boolean;
            RequireNumbers: boolean;
        };
    };
    createCustomerResponse: any;
    deleteCustomerResponse: any;
    listCustomersResponse: any;
    error: any;
};
declare const before00Setup: (testObject: DeleteCustomerTestObject) => Promise<void>;
declare const before01Run: (testObject: DeleteCustomerTestObject) => Promise<void>;
declare const before02CollectResults: (testObject: DeleteCustomerTestObject) => Promise<void>;
declare const after00TearDown: (testObject: DeleteCustomerTestObject) => Promise<void>;
declare const test00: (testObject: DeleteCustomerTestObject) => void;
export { before00Setup, before01Run, before02CollectResults, test00, after00TearDown };
