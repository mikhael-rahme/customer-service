"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
const index_1 = require("../createCustomer/index");
const index_2 = require("../listCustomers/index");
const { describe, it } = require('mocha');
const expect = require('chai').expect;
exports.createTestObject = () => ({
    createCustomerRequest: {
        customerName: `testCustomer-${Date.now()}`,
        passwordPolicy: {
            MinimumLength: 10,
            RequireLowercase: true,
            RequireSymbols: true,
            RequireUppercase: true,
            RequireNumbers: true,
        },
    },
    createCustomerResponse: null,
    deleteCustomerResponse: null,
    listCustomersResponse: null,
    error: null,
});
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.createCustomerResponse = yield index_1.default(testObject.createCustomerRequest);
        }
        catch (err) {
            console.log(err);
            testObject.error = err;
        }
    });
};
exports.before00Setup = before00Setup;
const before01Run = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.deleteCustomerResponse = yield _1.default(testObject.createCustomerResponse);
    }
    catch (err) {
        console.log(err);
        testObject.error = err;
    }
});
exports.before01Run = before01Run;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listCustomersResponse = yield index_2.default();
    }
    catch (err) {
        testObject.error = err;
    }
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('deleteCustomer', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('should return true', () => {
            expect(testObject.deleteCustomerResponse.result).to.be.true;
        });
        it('listCustomers should not include deletedCustomer', () => {
            const deletedCustomer = {
                customerName: testObject.createCustomerResponse.customerName,
                customerId: testObject.createCustomerResponse.customerId,
            };
            expect(testObject.listCustomersResponse).to.not.deep.include(deletedCustomer);
        });
    });
};
exports.test00 = test00;
