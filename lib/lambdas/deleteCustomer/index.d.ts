import { DeleteCustomerRequest, DeleteCustomerResponse } from './types';
declare const deleteCustomer: (request: DeleteCustomerRequest) => Promise<DeleteCustomerResponse>;
export default deleteCustomer;
