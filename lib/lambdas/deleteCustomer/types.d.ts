export declare type DeleteCustomerRequest = {
    customerName: string;
};
export declare type DeleteCustomerResponse = {
    result: boolean;
};
