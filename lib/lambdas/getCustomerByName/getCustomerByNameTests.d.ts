import { GetCustomerByNameRequest, GetCustomerByNameResponse, CreateCustomerResponse, CreateCustomerRequest } from '../../Types';
export declare type GetCustomerTestObject = {
    error: Error;
    getCustomerByNameRequest: GetCustomerByNameRequest;
    getCustomerByNameResponse: GetCustomerByNameResponse;
    createCustomerRequest: CreateCustomerRequest;
    createCustomerResponse: CreateCustomerResponse;
};
declare const createTestObject: () => GetCustomerTestObject;
declare const before00Setup: (testObject: GetCustomerTestObject) => Promise<void>;
declare const before01Run: (testObject: GetCustomerTestObject) => Promise<void>;
declare const before02CollectResults: (testObject: GetCustomerTestObject) => Promise<void>;
declare const after00TearDown: (testObject: GetCustomerTestObject) => Promise<void>;
declare const test00: (testObject: GetCustomerTestObject) => void;
export { createTestObject, before00Setup, before01Run, before02CollectResults, test00, after00TearDown };
