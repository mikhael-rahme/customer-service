import { Customer } from '../../Types';
export declare type GetCustomerByNameRequest = {
    customerName: string;
};
export declare type GetCustomerByNameResponse = Customer;
