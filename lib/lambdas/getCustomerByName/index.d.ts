import { GetCustomerByNameRequest } from './types';
declare const getCustomerByName: (request: GetCustomerByNameRequest) => Promise<{
    customerId: string;
    customerName: string;
    clientId?: string;
    enabled: boolean;
}>;
export default getCustomerByName;
