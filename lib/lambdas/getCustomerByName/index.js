"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const listCustomers_1 = require("../listCustomers");
const getCustomerByName = (request) => __awaiter(this, void 0, void 0, function* () {
    const { customerName } = request;
    const cognito = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.AWS_REGION || 'us-east-1',
    });
    const listCustomersResponse = yield listCustomers_1.default();
    const filteredCustomers = listCustomersResponse.filter(customer => customer.customerName === customerName);
    if (filteredCustomers.length !== 1) {
        throw new Error(`The given customerName does not correspond to a known customer or refers to multiple customers`);
    }
    const customer = filteredCustomers[0];
    const listUserPoolClientsRequest = {
        UserPoolId: customer.customerId,
        MaxResults: 10,
    };
    const cognitoResponse = yield cognito.listUserPoolClients(listUserPoolClientsRequest).promise();
    if (cognitoResponse.UserPoolClients.length !== 1) {
        throw new Error('The given customerName does not have any corresponding UserPoolClients!');
    }
    customer['clientId'] = cognitoResponse.UserPoolClients[0].ClientId;
    return customer;
});
exports.default = getCustomerByName;
