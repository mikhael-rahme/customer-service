import { Customer } from '../../Types';
export declare type ListCustomersRequest = null;
export declare type ListCustomersResponse = Array<Customer>;
