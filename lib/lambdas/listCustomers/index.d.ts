declare const listCustomers: () => Promise<{
    customerId: string;
    customerName: string;
    clientId?: string;
    enabled: boolean;
}[]>;
export default listCustomers;
