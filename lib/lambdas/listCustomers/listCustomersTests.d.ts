import { ListCustomersResponse, CreateCustomerResponse, CreateCustomerRequest } from '../../Types';
export declare type ListCustomerTestObject = {
    error: Error;
    listCustomersResponse: ListCustomersResponse;
    createCustomerResponses: CreateCustomerResponse[];
    createCustomerRequests: CreateCustomerRequest[];
};
declare const createTestObject: () => ListCustomerTestObject;
declare const before00Setup: (testObject: ListCustomerTestObject) => Promise<void>;
declare const before01Run: (testObject: ListCustomerTestObject) => Promise<void>;
declare const before02CollectResults: (testObject: ListCustomerTestObject) => Promise<void>;
declare const after00TearDown: (testObject: ListCustomerTestObject) => Promise<void[]>;
declare const test00: (testObject: ListCustomerTestObject) => void;
export { createTestObject, before00Setup, before01Run, before02CollectResults, test00, after00TearDown };
