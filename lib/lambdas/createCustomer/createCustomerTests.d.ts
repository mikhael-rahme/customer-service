import { CreateCustomerTestObject } from './types';
export { CreateCustomerTestObject };
declare const createTestObject: () => CreateCustomerTestObject;
declare const before00Setup: (testObject: CreateCustomerTestObject) => Promise<void>;
declare const before01Run: (testObject: CreateCustomerTestObject) => Promise<void>;
declare const before02CollectResults: (testObject: CreateCustomerTestObject) => Promise<void>;
declare const after00TearDown: (testObject: CreateCustomerTestObject) => Promise<void>;
declare const test00: (testObject: CreateCustomerTestObject) => void;
export { before00Setup, before01Run, before02CollectResults, test00, after00TearDown, createTestObject };
