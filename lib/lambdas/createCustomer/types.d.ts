import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { Customer } from '../../Types';
export declare type DescribeUserPoolResponse = CognitoIdentityServiceProvider.Types.DescribeUserPoolResponse;
export declare type DescribeUserPoolClientResponse = CognitoIdentityServiceProvider.Types.DescribeUserPoolClientResponse;
export declare type PasswordPolicy = CognitoIdentityServiceProvider.Types.PasswordPolicyType;
export declare type CreateCustomerRequest = {
    customerName: string;
    passwordPolicy: PasswordPolicy;
};
export declare type CreateCustomerResponse = Customer;
export declare type CreateCustomerTestObject = {
    createCustomerRequest: CreateCustomerRequest;
    createCustomerResponse: CreateCustomerResponse;
    userPoolResponse: DescribeUserPoolResponse;
    userPoolClientResponse: DescribeUserPoolClientResponse;
    error: Error;
};
