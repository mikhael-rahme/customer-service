"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const aws_sdk_1 = require("aws-sdk");
const cognito = new aws_sdk_1.CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.AWS_REGION || 'us-east-1',
});
const createTestObject = () => ({
    createCustomerRequest: {
        customerName: `testCustomer-${Date.now()}`,
        passwordPolicy: {
            MinimumLength: 10,
            RequireLowercase: true,
            RequireSymbols: true,
            RequireUppercase: true,
            RequireNumbers: true,
        },
    },
    userPoolResponse: null,
    userPoolClientResponse: null,
    error: null,
    createCustomerResponse: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        // nada
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.createCustomerResponse = yield _1.default(testObject.createCustomerRequest);
        }
        catch (err) {
            testObject.error = err;
        }
    });
};
exports.before01Run = before01Run;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.userPoolResponse = yield cognito.describeUserPool({ UserPoolId: testObject.createCustomerResponse.customerId }).promise();
        testObject.userPoolClientResponse = yield cognito.describeUserPoolClient({
            UserPoolId: testObject.createCustomerResponse.customerId,
            ClientId: testObject.createCustomerResponse.clientId,
        }).promise();
    }
    catch (err) {
        testObject.error = err;
    }
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield cognito.deleteUserPoolClient({
        UserPoolId: testObject.createCustomerResponse.customerId,
        ClientId: testObject.createCustomerResponse.clientId,
    }).promise();
    yield cognito.deleteUserPool({
        UserPoolId: testObject.createCustomerResponse.customerId,
    }).promise();
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('createCustomer', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('describeUserPool should not return null', () => {
            expect(testObject.userPoolResponse).not.to.be.null;
        });
        it('describeUserPoolClient not return null', () => {
            expect(testObject.userPoolResponse).not.to.be.null;
        });
    });
};
exports.test00 = test00;
