import { CreateCustomerRequest } from './types';
declare const createCustomer: (request: CreateCustomerRequest) => Promise<{
    customerId: string;
    customerName: string;
    clientId?: string;
    enabled: boolean;
}>;
export default createCustomer;
