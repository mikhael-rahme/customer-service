"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const Types_1 = require("../../Types");
const createCustomer = (request) => __awaiter(this, void 0, void 0, function* () {
    const { customerName, passwordPolicy, } = request;
    const cognito = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.AWS_REGION || 'us-east-1',
    });
    const createUserPoolParameters = {
        PoolName: customerName,
        AdminCreateUserConfig: {
            AllowAdminCreateUserOnly: true,
        },
        Policies: {
            PasswordPolicy: passwordPolicy,
        },
        Schema: [
            {
                Name: 'email',
                AttributeDataType: 'String',
                Mutable: true,
                Required: true,
            },
        ],
    };
    const createUserPoolResponse = yield cognito.createUserPool(createUserPoolParameters).promise();
    const customer = Types_1.cognitoCustomerToCustomer(createUserPoolResponse.UserPool);
    const createUserPoolClientParameters = {
        ClientName: `${customerName}-web-client`,
        UserPoolId: createUserPoolResponse.UserPool.Id,
        AllowedOAuthFlowsUserPoolClient: false,
        // DefaultRedirectURI: `${customerName}.${productDomain}.com`,
        ExplicitAuthFlows: [
            'ADMIN_NO_SRP_AUTH',
        ],
        GenerateSecret: false,
    };
    const createUserPoolClientResponse = yield cognito.createUserPoolClient(createUserPoolClientParameters).promise();
    return Object.assign({}, customer, { clientId: createUserPoolClientResponse.UserPoolClient.ClientId });
});
exports.default = createCustomer;
