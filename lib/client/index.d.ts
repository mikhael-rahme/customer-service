import { ServiceResult, ServiceRequest } from 'toolkit';
import { CreateCustomerRequest, GetCustomerByNameRequest, DeleteCustomerRequest, DeleteCustomerResponse } from '../Types';
declare const createClient: (caller: string, stage: string, options?: {
    serviceRequest?: ServiceRequest;
    version?: string;
}) => {
    createCustomer: (request: CreateCustomerRequest) => Promise<ServiceResult<{
        customerId: string;
        customerName: string;
        clientId?: string;
        enabled: boolean;
    }>>;
    listCustomers: (request?: null) => Promise<ServiceResult<{
        customerId: string;
        customerName: string;
        clientId?: string;
        enabled: boolean;
    }[]>>;
    getCustomerByName: (request: GetCustomerByNameRequest) => Promise<ServiceResult<{
        customerId: string;
        customerName: string;
        clientId?: string;
        enabled: boolean;
    }>>;
    deleteCustomer: (request: DeleteCustomerRequest) => Promise<ServiceResult<DeleteCustomerResponse>>;
};
export default createClient;
