"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const toolkit_1 = require("toolkit");
const aws_sdk_1 = require("aws-sdk");
const { name: serviceName, version: serviceVersion, } = require('../../package.json');
// destructuring makes us lose the typing! sucks
const createLambdaRequest = toolkit_1.LambdaUtils.createLambdaRequest;
const createClient = (caller, stage, options = {}) => {
    const { serviceRequest, version, } = options;
    const lambda = new aws_sdk_1.Lambda({
        apiVersion: '2015-03-31',
        region: process.env.AWS_REGION || 'us-east-1',
    });
    const createLambdaRequestArguments = (functionName) => ({
        lambda,
        stage,
        serviceName,
        caller,
        functionName,
    });
    const createLambdaRequestOptions = (request) => ({
        serviceRequest,
        version: (version || serviceVersion).replace(/\./g, '-'),
        payload: request,
    });
    return {
        createCustomer: (request) => createLambdaRequest(createLambdaRequestArguments('createCustomer'), createLambdaRequestOptions(request)),
        listCustomers: (request) => createLambdaRequest(createLambdaRequestArguments('listCustomers'), createLambdaRequestOptions(request)),
        getCustomerByName: (request) => createLambdaRequest(createLambdaRequestArguments('getCustomerByName'), createLambdaRequestOptions(request)),
        deleteCustomer: (request) => createLambdaRequest(createLambdaRequestArguments('deleteCustomer'), createLambdaRequestOptions(request)),
    };
};
exports.default = createClient;
