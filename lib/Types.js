"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cognitoCustomerToCustomer = (customer) => ({
    customerId: customer.Id,
    customerName: customer.Name,
    enabled: customer.Status === 'Enabled',
});
