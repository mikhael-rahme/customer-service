import{
    Answers,
    prompt,
} from 'inquirer';

import listCustomers from '../lambdas/listCustomers';
import getCustomerByName from '../lambdas/getCustomerByName';
import createCustomer from '../lambdas/createCustomer';
import deleteCustomer from '../lambdas/deleteCustomer';
import * as Table from 'cli-table';

const MenuItems = {
    listCustomers: async () => {

      try{
        const listCustomersResponse: Answers = await prompt([
          {
            type: 'default',
            name: 'createList',
            message: 'List Created!',
          },
        ]);
        const list = await listCustomers();
        const head = ['Customer ID', 'Customer Name', 'Enabled'];
        const table = new Table({ head });
        table.push(...list.map(Customer => [
          Customer.customerId,
          Customer.customerName,
          Customer.enabled,
        ]));
        console.log(table.toString());
      }
      catch (err){
        console.log(err);
      }
    },

    getCustomer: async () => {
      try{
        const customerNameAnswer: Answers = await prompt([
          {
            type: 'input',
            name: 'customerName',
            message: 'Enter customer name',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const customerName = customerNameAnswer['customerName'];
        const customer = await getCustomerByName({
          customerName,
        });
        
        const head = ['Client ID', 'User Pool ID', 'Client Name'];
        const table = new Table({ head });
        table.push([
          customer.clientId,
          customer.customerId,
          customer.customerName,
        ]);
        console.log(table.toString());
      }
      catch (err){
        console.log(err);
      }
    },

    createCustomer: async () => {
      try{
        const createCustomerAnswer: Answers = await prompt([
          {
            type: 'input',
            name: 'newCustomerName',
            message: 'Enter new customer name',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const passwordPolicyAnswer: Answers = await prompt([
          {
            type: 'input',
            name: 'newPasswordPolicy',
            message: 'Enter new password policy',
            validate: input => !!input || 'You must give a value!',
          }
        ]);

        const newCustomerName = createCustomerAnswer['newCustomerName'];
        const passwordpolicy = passwordPolicyAnswer['newPasswordPolicy'];

      }
      catch (err){
        console.log(err);
      }
    },

    deleteExistingCustomer: async () => {
      try{
        const deleteCustomerAnswer: Answers = await prompt([
          {
            type: 'input',
            name: 'deleteCustomer',
            message: 'Enter customer name to delete',
            validate: input => !!input || 'You must give a value!',
          }
        ]);

        const deleteExistingCustomer = deleteCustomerAnswer['deleteCustomer'];

        const deletedExistingCustomer = await deleteCustomer(
          deleteExistingCustomer,
        );
        const head = ['Customer Deleted'];
        const table = new Table({ head });
        table.push(deletedExistingCustomer);
        console.log(table.toString());

      }
      catch (err){
        console.log(err);
      }
    }
};

export default MenuItems;