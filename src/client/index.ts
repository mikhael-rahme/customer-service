import {
  ServiceResult,
  ServiceRequest,
  LambdaUtils,
} from 'toolkit';
import {
  CreateCustomerRequest,
  CreateCustomerResponse,
  GetCustomerByNameRequest,
  GetCustomerByNameResponse,
  ListCustomersRequest,
  ListCustomersResponse,
  DeleteCustomerRequest,
  DeleteCustomerResponse,
} from '../Types';
import { Lambda } from 'aws-sdk';
const {
  name: serviceName,
  version: serviceVersion,
} = require('../../package.json');

// destructuring makes us lose the typing! sucks
const createLambdaRequest = LambdaUtils.createLambdaRequest;

const createClient = (
  caller: string, 
  stage: string, 
  options: {
    serviceRequest?: ServiceRequest,
    version?: string,
  } = {}) => {
  const {
    serviceRequest,
    version,
  } = options;

  const lambda = new Lambda({
    apiVersion: '2015-03-31',
    region: process.env.AWS_REGION || 'us-east-1',
  });
  const createLambdaRequestArguments = (functionName: string) => ({
    lambda,
    stage,
    serviceName,
    caller,
    functionName,
  });

  const createLambdaRequestOptions = <T>(request: T) => ({
    serviceRequest,
    version: (version || serviceVersion).replace(/\./g, '-'),
    payload: request,
  });
  return {
    createCustomer:(request: CreateCustomerRequest) =>
      createLambdaRequest<CreateCustomerRequest, CreateCustomerResponse>
        (createLambdaRequestArguments('createCustomer'), createLambdaRequestOptions(request)),
    listCustomers: (request?: ListCustomersRequest) =>
      createLambdaRequest<ListCustomersRequest, ListCustomersResponse>
        (createLambdaRequestArguments('listCustomers'), createLambdaRequestOptions(request)),
    getCustomerByName: (request: GetCustomerByNameRequest) =>
      createLambdaRequest<GetCustomerByNameRequest, GetCustomerByNameResponse>
        (createLambdaRequestArguments('getCustomerByName'), createLambdaRequestOptions(request)),
    deleteCustomer: (request: DeleteCustomerRequest) =>
      createLambdaRequest<DeleteCustomerRequest, DeleteCustomerResponse>
        (createLambdaRequestArguments('deleteCustomer'), createLambdaRequestOptions(request)),
  };
};

export default createClient;
