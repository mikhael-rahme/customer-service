const { describe, it, before, after } = require('mocha');
const expect = require('chai').expect;
import createClient from '../../client';
import { ServiceResult, testUtils } from 'toolkit';
import {
  GetCustomerByNameResponse,
} from '../../Types';

import {
  test00,
  after00TearDown,
  before00Setup,
  before02CollectResults,
  createTestObject,
} from './getCustomerByNameTests';

const caller = 'integrationTestCaller';
const stage = testUtils.getTestStage();

const getCustomerByNameTestObject = createTestObject();

describe('getCustomerByName:', () => {
  describe('invoking with one parameters', function () {
    this.timeout(20000);
    let response: ServiceResult<GetCustomerByNameResponse> = null;
    before(async () => before00Setup(getCustomerByNameTestObject));
    before(async () => {
      try {
        const client = createClient(caller, stage);
        response =
          await client.getCustomerByName(getCustomerByNameTestObject.createCustomerRequest);
        getCustomerByNameTestObject.getCustomerByNameResponse = response.data;
      } catch (err) {
        console.log(`err: ${JSON.stringify(err.message, null, 2)}`);
        getCustomerByNameTestObject.error = err;
      }
    });

    before(async () => before02CollectResults(getCustomerByNameTestObject));
    test00(getCustomerByNameTestObject);

    it('should not return null', () => {
      expect(response).not.to.be.null;
    });
    it('should return data', () => {
      expect(response.data).not.to.be.undefined;
    });
    it('should not return an error', () => {
      expect(response.error).to.be.undefined;
    });
    after(async () => after00TearDown(getCustomerByNameTestObject));
  });
});
