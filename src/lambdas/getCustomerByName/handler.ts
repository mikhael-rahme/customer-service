import {
  Context,
} from 'aws-lambda';
import getCustomerByName from './index';
import { GetCustomerByNameResponse, GetCustomerByNameRequest } from './types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler<GetCustomerByNameRequest, GetCustomerByNameResponse>(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: GetCustomerByNameRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: GetCustomerByNameResponse = await getCustomerByName(request);
    return new ServiceResult(serviceRequest, { data });
  });


