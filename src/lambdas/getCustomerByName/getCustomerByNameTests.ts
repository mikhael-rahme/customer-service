import getCustomer from './';
import createCustomer from '../createCustomer/index';
import {
  GetCustomerByNameRequest,
  GetCustomerByNameResponse,
  CreateCustomerResponse,
  CreateCustomerRequest,
} from '../../Types';
const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';

const cognito = new CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
  region: process.env.AWS_REGION || 'us-east-1',
});

export type GetCustomerTestObject = {
  error: Error,
  getCustomerByNameRequest: GetCustomerByNameRequest,
  getCustomerByNameResponse: GetCustomerByNameResponse,
  createCustomerRequest: CreateCustomerRequest,
  createCustomerResponse: CreateCustomerResponse,
};


const createTestObject = (): GetCustomerTestObject => ({
  createCustomerRequest: {
    customerName: `testCustomer-${Date.now()}`,
    passwordPolicy: {
      MinimumLength: 10,
      RequireLowercase: true,
      RequireSymbols: true,
      RequireUppercase: true,
      RequireNumbers: true,
    },
  },
  getCustomerByNameResponse: null,
  getCustomerByNameRequest: null,
  createCustomerResponse: null,
  error: null,
});

const before00Setup = async function (testObject: GetCustomerTestObject) {
  try {
    testObject.createCustomerResponse = await createCustomer(testObject.createCustomerRequest);
    testObject.getCustomerByNameRequest = { customerName: testObject.createCustomerResponse.customerName };
  } catch (err) {
    testObject.error = err;
  }
};

const before01Run = async (testObject: GetCustomerTestObject) => {
  try {
    testObject.getCustomerByNameResponse = await getCustomer(testObject.getCustomerByNameRequest);
  } catch (err) {
    testObject.error = err;
  }
};

const before02CollectResults = async (testObject: GetCustomerTestObject) => {
  // nada
};

const after00TearDown = async (testObject: GetCustomerTestObject) => {
  await cognito.deleteUserPoolClient({
    UserPoolId: testObject.createCustomerResponse.customerId,
    ClientId: testObject.createCustomerResponse.clientId,
  }).promise();
  await cognito.deleteUserPool({
    UserPoolId: testObject.createCustomerResponse.customerId,
  }).promise();
};

const test00 = (testObject: GetCustomerTestObject) => {
  describe('getCustomerByName', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('should not have returned null', () => {
      expect(testObject.getCustomerByNameResponse).to.not.be.null;
    });
    it('should return the test customer', () => {
      expect(testObject.getCustomerByNameResponse).to.deep.equal(testObject.createCustomerResponse);
    });
  });
};

export {
  createTestObject,
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
};
