import { Customer } from '../../Types';

export type GetCustomerByNameRequest = {
  customerName: string;
};

export type GetCustomerByNameResponse = Customer;
