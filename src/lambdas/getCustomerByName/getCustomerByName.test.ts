import {
  createTestObject,
  GetCustomerTestObject,
  after00TearDown,
  test00,
  before00Setup,
  before01Run,
  before02CollectResults,
} from './getCustomerByNameTests';

const { before, after } = require('mocha');

const getCustomerTestObject: GetCustomerTestObject = createTestObject();

describe('getCustomerByName', async function () {
  this.timeout(20000);
  before(async () => before00Setup(getCustomerTestObject));
  before(async () => before01Run(getCustomerTestObject));
  before(async () => before02CollectResults(getCustomerTestObject));
  test00(getCustomerTestObject);
  after(async () => after00TearDown(getCustomerTestObject));
});
