import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { GetCustomerByNameRequest, GetCustomerByNameResponse } from './types';
import listCustomers from '../listCustomers';

const getCustomerByName = async (request: GetCustomerByNameRequest): Promise<GetCustomerByNameResponse> => {
  const { customerName } = request;
  const cognito = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.AWS_REGION || 'us-east-1',
  });

  const listCustomersResponse = await listCustomers();
  const filteredCustomers = listCustomersResponse.filter(customer => customer.customerName === customerName);
  if (filteredCustomers.length !== 1) {
    throw new Error(`The given customerName does not correspond to a known customer or refers to multiple customers`);
  }

  const customer = filteredCustomers[0];

  const listUserPoolClientsRequest: CognitoIdentityServiceProvider.ListUserPoolClientsRequest = {
    UserPoolId: customer.customerId,
    MaxResults: 10,
  };
  const cognitoResponse = await cognito.listUserPoolClients(listUserPoolClientsRequest).promise();

  if (cognitoResponse.UserPoolClients.length !== 1) {
    throw new Error('The given customerName does not have any corresponding UserPoolClients!');
  }

  customer['clientId'] = cognitoResponse.UserPoolClients[0].ClientId;

  return customer;
};

export default getCustomerByName;
