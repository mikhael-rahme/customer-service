import {
  Context,
} from 'aws-lambda';
import listCustomers from './index';
import { ListCustomersResponse } from './types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler<ListCustomersResponse, void>(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request, // ignored for now, will be used when permissions are implemented
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: ListCustomersResponse = await listCustomers();
    return new ServiceResult(serviceRequest, { data });
  });
