const { describe, it, before, after } = require('mocha');
const expect = require('chai').expect;
import createClient from '../../client';
import { ServiceResult, testUtils } from 'toolkit';
import { ListCustomersRequest, ListCustomersResponse } from '../../Types';

import {
  test00,
  after00TearDown,
  before00Setup,
  before02CollectResults,
  createTestObject,
} from './listCustomersTests';

const caller = 'integrationTestCaller';
const stage = testUtils.getTestStage();
const listCustomerTestObject = createTestObject();

describe('listCustomers:', () => {
  describe('invoking with one parameters', function () {
    this.timeout(20000);
    let response = null;
    let error = null;
    before(async () => before00Setup(listCustomerTestObject));
    before(async () => {
      try {
        const client = createClient(caller, stage);
        response = await client.listCustomers();
        listCustomerTestObject.listCustomersResponse = response.data;

      } catch (err) {
        console.log(`err: ${JSON.stringify(err.message, null, 2)}`);
        error = null;
      }
    });
    before(async () => before02CollectResults(listCustomerTestObject));
    test00(listCustomerTestObject);

    it('should not return null', () => {
      expect(response).not.to.be.null;
    });
    it('should return data', () => {
      expect(response.data).not.to.be.undefined;
    });
    it('should not return an error', () => {
      expect(response.error).to.be.undefined;
    });
    after(async () => after00TearDown(listCustomerTestObject));
  });
});
