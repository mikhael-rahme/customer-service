import {
  createTestObject,
  ListCustomerTestObject,
  after00TearDown,
  test00,
  before00Setup,
  before01Run,
  before02CollectResults,
} from './listCustomersTests';

const { before, after } = require('mocha');

const listCustomerTestObject: ListCustomerTestObject = createTestObject();

const createTestsForListCustomers = async () => {
  before(async () => before00Setup(listCustomerTestObject));
  before(async () => before01Run(listCustomerTestObject));
  before(async () => before02CollectResults(listCustomerTestObject));
  test00(listCustomerTestObject);
  after(async () => after00TearDown(listCustomerTestObject));
};

createTestsForListCustomers();
