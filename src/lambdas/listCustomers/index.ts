import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ListCustomersResponse } from './types';
import { cognitoCustomerToCustomer } from '../../Types';

const listCustomers = async (): Promise<ListCustomersResponse> => {
  const cognito = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.AWS_REGION || 'us-east-1',
  });
  await cognito.listUserPools({
    MaxResults: 60, // all in account
  }).promise();
  const userPoolResponse: CognitoIdentityServiceProvider.Types.ListUserPoolsResponse = await cognito.listUserPools({
    MaxResults: 60, // all in account
  }).promise();
  return userPoolResponse.UserPools.map(cognitoCustomerToCustomer);
};

export default listCustomers;
