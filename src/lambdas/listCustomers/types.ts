import { Customer } from '../../Types';
export type ListCustomersRequest = null;
export type ListCustomersResponse = Array<Customer>;
