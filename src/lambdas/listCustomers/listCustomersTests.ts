import listCustomers from './';
import createCustomer from '../createCustomer/index';
import {
  ListCustomersResponse,
  CreateCustomerResponse,
  CreateCustomerRequest,
} from '../../Types';
const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';

const cognito = new CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
  region: process.env.AWS_REGION || 'us-east-1',
});

export type ListCustomerTestObject = {
  error: Error,
  listCustomersResponse: ListCustomersResponse,
  createCustomerResponses: CreateCustomerResponse[],
  createCustomerRequests: CreateCustomerRequest[],
};


const createTestObject = (): ListCustomerTestObject => ({
  createCustomerRequests: ['testA', 'testB', 'testC'].map(name => ({
    customerName: `${name}-${Date.now()}`,
    passwordPolicy: {
      MinimumLength: 10,
      RequireLowercase: true,
      RequireSymbols: true,
      RequireUppercase: true,
      RequireNumbers: true,
    },
  })),
  createCustomerResponses: null,
  listCustomersResponse: null,
  error: null,
});

const before00Setup = async function (testObject: ListCustomerTestObject) {
  try {
    testObject.createCustomerResponses = await Promise.all(testObject.createCustomerRequests.map(req => createCustomer(req)));
  } catch (err) {
    testObject.error = err;
  }
};

const before01Run = async (testObject: ListCustomerTestObject) => {
  try {
    testObject.listCustomersResponse = await listCustomers();
  } catch (err) {
    testObject.error = err;
  }
};

const before02CollectResults = async (testObject: ListCustomerTestObject) => {
  // nada
};

const after00TearDown = async (testObject: ListCustomerTestObject) => {
  return Promise.all(testObject.createCustomerResponses.map(async (customerResponse) => {
    await cognito.deleteUserPoolClient({
      UserPoolId: customerResponse.customerId,
      ClientId: customerResponse.clientId,
    }).promise();
    await cognito.deleteUserPool({
      UserPoolId: customerResponse.customerId,
    }).promise();
  }));
};

const test00 = (testObject: ListCustomerTestObject) => {
  describe('listCustomer', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('should not have returned null', () => {
      expect(testObject.listCustomersResponse).to.not.be.null;
    });
    it('should return at least the list of test customers', () => {
      const testFor = testObject.createCustomerResponses.map(response => ({
        customerName: response.customerName,
        customerId: response.customerId,
        enabled: response.enabled,
      }));
      expect(testObject.listCustomersResponse).to.deep.include(...testFor);
    });
  });
};

export {
  createTestObject,
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
};
