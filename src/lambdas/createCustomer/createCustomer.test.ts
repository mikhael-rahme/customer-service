import {
  before00Setup,
  before01Run,
  before02CollectResults,
  after00TearDown,
  test00,
  createTestObject,
  CreateCustomerTestObject,
} from './createCustomerTests';

const testObject: CreateCustomerTestObject = createTestObject();

describe('createCustomer', function () {
  this.timeout(20000);
  // Setup
  before(async () => before00Setup(testObject));

  // Run
  before(async () => before01Run(testObject));

  // CollectResults
  before(async () => before02CollectResults(testObject));

  // VerifyResults
  test00(testObject);

  // Cleanup
  after(async () => after00TearDown(testObject));
});
