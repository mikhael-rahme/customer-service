import createCustomer from './';
import { CreateCustomerTestObject } from './types';
const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';

const cognito = new CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
  region: process.env.AWS_REGION || 'us-east-1',
});

export {
  CreateCustomerTestObject,
};

const createTestObject = (): CreateCustomerTestObject => ({
  createCustomerRequest: {
    customerName: `testCustomer-${Date.now()}`,
    passwordPolicy: {
      MinimumLength: 10,
      RequireLowercase: true,
      RequireSymbols: true,
      RequireUppercase: true,
      RequireNumbers: true,
    },
  },
  userPoolResponse: null,
  userPoolClientResponse: null,
  error: null,
  createCustomerResponse: null,
});

const before00Setup = async function (testObject: CreateCustomerTestObject) {
  // nada
};

const before01Run = async function (testObject: CreateCustomerTestObject) {
  try {
    testObject.createCustomerResponse = await createCustomer(testObject.createCustomerRequest);
  } catch (err) {
    testObject.error = err;
  }
};
const before02CollectResults = async (testObject: CreateCustomerTestObject) => {
  try {
    testObject.userPoolResponse = await cognito.describeUserPool({ UserPoolId: testObject.createCustomerResponse.customerId }).promise();
    testObject.userPoolClientResponse = await cognito.describeUserPoolClient({
      UserPoolId: testObject.createCustomerResponse.customerId,
      ClientId: testObject.createCustomerResponse.clientId,
    }).promise();

  } catch (err) {
    testObject.error = err;
  }
};

const after00TearDown = async (testObject: CreateCustomerTestObject) => {
  await cognito.deleteUserPoolClient({
    UserPoolId: testObject.createCustomerResponse.customerId,
    ClientId: testObject.createCustomerResponse.clientId,
  }).promise();
  await cognito.deleteUserPool({
    UserPoolId: testObject.createCustomerResponse.customerId,
  }).promise();
};

const test00 = (testObject: CreateCustomerTestObject) => {
  describe('createCustomer', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('describeUserPool should not return null', () => {
      expect(testObject.userPoolResponse).not.to.be.null;
    });
    it('describeUserPoolClient not return null', () => {
      expect(testObject.userPoolResponse).not.to.be.null;
    });
  });
};

export {
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
