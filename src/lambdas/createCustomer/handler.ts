import {
  Context,
} from 'aws-lambda';
import createCustomer from './index';
import { CreateCustomerResponse, CreateCustomerRequest } from './types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler<CreateCustomerRequest, CreateCustomerResponse>(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: CreateCustomerRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: CreateCustomerResponse = await createCustomer(request);
    return new ServiceResult(serviceRequest, { data });
  });

