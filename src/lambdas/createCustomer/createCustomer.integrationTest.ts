const { describe, it, before, after } = require('mocha');
const expect = require('chai').expect;
import createClient from '../../client';
import { ServiceResult, testUtils } from 'toolkit';
import {
  CreateCustomerResponse,
} from '../../Types';

import {
  test00,
  after00TearDown,
  before00Setup,
  before02CollectResults,
  createTestObject,
} from './createCustomerTests';

const caller = 'integrationTestCaller';

const createCustomerTestObject = createTestObject();

describe('createCustomer:', () => {
  describe('invoking with one parameters', function () {
    this.timeout(20000);
    let response: ServiceResult<CreateCustomerResponse> = null;
    before(async () => before00Setup(createCustomerTestObject));
    before(async () => {
      try {
        const client = createClient(caller, testUtils.getTestStage());
        response = await client.createCustomer(createCustomerTestObject.createCustomerRequest);
        createCustomerTestObject.createCustomerResponse = response.data;
      } catch (err) {
        console.log(`err: ${JSON.stringify(err.message, null, 2)}`);
        createCustomerTestObject.error = err;
      }
    });

    before(async () => before02CollectResults(createCustomerTestObject));
    test00(createCustomerTestObject);

    it('should not return null', () => {
      expect(response).not.to.be.null;
    });
    it('should return data', () => {
      expect(response.data).not.to.be.undefined;
    });
    it('should not return an error', () => {
      expect(response.error).to.be.undefined;
    });
    after(async () => after00TearDown(createCustomerTestObject));
  });
});
