import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { CreateCustomerRequest, CreateCustomerResponse } from './types';
import { cognitoCustomerToCustomer } from '../../Types';

const createCustomer = async (request: CreateCustomerRequest): Promise<CreateCustomerResponse> => {
  const {
    customerName,
    passwordPolicy,
  } = request;

  const cognito = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.AWS_REGION || 'us-east-1',
  });
  const createUserPoolParameters: CognitoIdentityServiceProvider.Types.CreateUserPoolRequest = {
    PoolName: customerName,
    AdminCreateUserConfig: {
      AllowAdminCreateUserOnly: true,
    },
    Policies: {
      PasswordPolicy: passwordPolicy,
    },
    Schema: [
      {
        Name: 'email',
        AttributeDataType: 'String',
        Mutable: true,
        Required: true,
      },
    ],
  };
  const createUserPoolResponse: CognitoIdentityServiceProvider.Types.CreateUserPoolResponse =
    await cognito.createUserPool(createUserPoolParameters).promise();
  const customer = cognitoCustomerToCustomer(createUserPoolResponse.UserPool);

  const createUserPoolClientParameters: CognitoIdentityServiceProvider.Types.CreateUserPoolClientRequest = {
    ClientName: `${customerName}-web-client`,
    UserPoolId: createUserPoolResponse.UserPool.Id,
    AllowedOAuthFlowsUserPoolClient: false,
    // DefaultRedirectURI: `${customerName}.${productDomain}.com`,
    ExplicitAuthFlows: [
      'ADMIN_NO_SRP_AUTH',
    ],
    GenerateSecret: false,
    // CallbackURLs: [
    //   `${customerName}.${productDomain}.com/logout`,
    //   `${customerName}.${productDomain}.com`,
    // ],
    // LogoutURLs: [
    //  `${customerName}.${productDomain}.com/logout`,
    // ],
  };

  const createUserPoolClientResponse: CognitoIdentityServiceProvider.Types.CreateUserPoolClientResponse =
    await cognito.createUserPoolClient(createUserPoolClientParameters).promise();



  return {
    ...customer,
    clientId: createUserPoolClientResponse.UserPoolClient.ClientId,
  };
};

export default createCustomer;
