import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { Customer } from '../../Types';

export type DescribeUserPoolResponse = CognitoIdentityServiceProvider.Types.DescribeUserPoolResponse;
export type DescribeUserPoolClientResponse = CognitoIdentityServiceProvider.Types.DescribeUserPoolClientResponse;
export type PasswordPolicy = CognitoIdentityServiceProvider.Types.PasswordPolicyType;

export type CreateCustomerRequest = {
  customerName: string,
  passwordPolicy: PasswordPolicy,
};

export type CreateCustomerResponse = Customer;

export type CreateCustomerTestObject = {
  createCustomerRequest: CreateCustomerRequest,
  createCustomerResponse: CreateCustomerResponse,
  userPoolResponse: DescribeUserPoolResponse,
  userPoolClientResponse: DescribeUserPoolClientResponse,
  error: Error,
};
