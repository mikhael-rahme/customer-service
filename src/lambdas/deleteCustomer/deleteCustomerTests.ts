import deleteCustomer from './';
import createCustomer from '../createCustomer/index';
import listCustomers from '../listCustomers/index';
import {
  ListCustomersResponse,
  CreateCustomerResponse,
  CreateCustomerRequest,
  DeleteCustomerResponse,
} from '../../Types';
const { describe, it } = require('mocha');
const expect = require('chai').expect;

export type DeleteCustomerTestObject = {
  createCustomerRequest: CreateCustomerRequest,
  createCustomerResponse: CreateCustomerResponse,
  deleteCustomerResponse: DeleteCustomerResponse,
  listCustomersResponse: ListCustomersResponse,
  error: Error,
};

export const createTestObject = () => ({
  createCustomerRequest: {
    customerName: `testCustomer-${Date.now()}`,
    passwordPolicy: {
      MinimumLength: 10,
      RequireLowercase: true,
      RequireSymbols: true,
      RequireUppercase: true,
      RequireNumbers: true,
    },
  },
  createCustomerResponse: null,
  deleteCustomerResponse: null,
  listCustomersResponse: null,
  error: null,
});

const before00Setup = async function (testObject: DeleteCustomerTestObject) {
  try {
    testObject.createCustomerResponse = await createCustomer(testObject.createCustomerRequest);
  } catch (err) {
    console.log(err);
    testObject.error = err;
  }
};

const before01Run = async (testObject: DeleteCustomerTestObject) => {
  try {
    testObject.deleteCustomerResponse = await deleteCustomer(testObject.createCustomerResponse);
  } catch (err) {
    console.log(err);
    testObject.error = err;
  }
};

const before02CollectResults = async (testObject: DeleteCustomerTestObject) => {
  try {
    testObject.listCustomersResponse = await listCustomers();
  } catch (err) {
    testObject.error = err;
  }
};

const after00TearDown = async (testObject: DeleteCustomerTestObject) => {
};

const test00 = (testObject: DeleteCustomerTestObject) => {
  describe('deleteCustomer', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('should return true', () => {
      expect(testObject.deleteCustomerResponse.result).to.be.true;
    });
    it('listCustomers should not include deletedCustomer', () => {
      const deletedCustomer = {
        customerName: testObject.createCustomerResponse.customerName,
        customerId: testObject.createCustomerResponse.customerId,
      };
      expect(testObject.listCustomersResponse).to.not.deep.include(deletedCustomer);
    });
  });
};

export {
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
};
