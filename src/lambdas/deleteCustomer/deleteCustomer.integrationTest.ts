const { describe, it, before, after } = require('mocha');
const expect = require('chai').expect;
import createClient from '../../client';
import { ServiceResult, testUtils } from 'toolkit';
import {
  DeleteCustomerResponse,
} from '../../Types';

import {
  test00,
  after00TearDown,
  before00Setup,
  before02CollectResults,
  createTestObject,
} from './deleteCustomerTests';

const stage = testUtils.getTestStage();
const caller = 'integrationTestCaller';

const deleteCustomerTestObject = createTestObject();

describe('deleteCustomer:', () => {
  describe('invoking with one parameters', function () {
    this.timeout(20000);
    let response: ServiceResult<DeleteCustomerResponse> = null;
    before(async () => before00Setup(deleteCustomerTestObject));
    before(async () => {
      try {
        const client = createClient(caller, stage);
        response = await client.deleteCustomer(deleteCustomerTestObject.createCustomerRequest);
        deleteCustomerTestObject.deleteCustomerResponse = response.data;
      } catch (err) {
        console.log(`err: ${JSON.stringify(err.message, null, 2)}`);
        deleteCustomerTestObject.error = err;
      }
    });

    before(async () => before02CollectResults(deleteCustomerTestObject));
    test00(deleteCustomerTestObject);

    it('should not return null', () => {
      expect(response).not.to.be.null;
    });
    it('should return data', () => {
      expect(response.data).not.to.be.undefined;
    });
    it('should not return an error', () => {
      expect(response.error).to.be.undefined;
    });
    after(async () => after00TearDown(deleteCustomerTestObject));
  });
});
