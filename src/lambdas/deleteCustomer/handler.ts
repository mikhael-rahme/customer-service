import {
  Context,
} from 'aws-lambda';
import deleteCustomer from './index';
import { DeleteCustomerRequest, DeleteCustomerResponse } from './types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler<DeleteCustomerRequest, DeleteCustomerResponse>(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: DeleteCustomerRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: DeleteCustomerResponse = await deleteCustomer(request);
    return new ServiceResult(serviceRequest, { data });
  });
