import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { DeleteCustomerRequest, DeleteCustomerResponse } from './types';

const deleteCustomer = async (request: DeleteCustomerRequest): Promise<DeleteCustomerResponse> => {
  const { customerName } = request;
  const cognito = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.AWS_REGION || 'us-east-1',
  });

  const filteredCustomers = (await cognito.listUserPools({ MaxResults: 60 })
    .promise()).UserPools.filter(pool => pool.Name === customerName);
  if (filteredCustomers.length !== 1) {
    throw new Error(`The given customerName does not correspond to a known customer or refers to multiple customers`);
  }
  const customerId = filteredCustomers[0].Id;

  await cognito.deleteUserPool({
    UserPoolId: customerId,
  }).promise();

  return { result: true };
};

export default deleteCustomer;
