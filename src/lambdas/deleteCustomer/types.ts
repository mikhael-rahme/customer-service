export type DeleteCustomerRequest = {
  customerName: string;
};
export type DeleteCustomerResponse = {
  result: boolean;
};
