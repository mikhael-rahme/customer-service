import { UserPoolDescriptionType } from 'aws-sdk/clients/cognitoidentityserviceprovider';

export * from './lambdas/createCustomer/types';
export * from './lambdas/listCustomers/types';
export * from './lambdas/deleteCustomer/types';
export * from './lambdas/getCustomerByName/types';

export const cognitoCustomerToCustomer = (customer: UserPoolDescriptionType) => ({
  customerId: customer.Id,
  customerName: customer.Name,
  enabled: customer.Status === 'Enabled',
});

export type Customer = {
  customerId: string,
  customerName: string,
  clientId?: string,
  enabled: boolean,
};
