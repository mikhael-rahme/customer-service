const tests = require('./lib/tests');
const createClient = require('./lib/client').default;

exports.default = createClient;
exports.tests = tests;
