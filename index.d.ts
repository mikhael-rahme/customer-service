import createClient from './lib/client';
export * from './src/Types';
export import tests = require('./src/tests');

export default createClient;
